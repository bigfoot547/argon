#include "argon.h"

#include <iostream>

class ModuleExample : public Module
{
public:
	void Initialize() override
	{
		std::cout << "ModuleExample loaded! -ModuleExample::Initialize" << std::endl;
	}
};

MODULE_INIT(ModuleExample)
