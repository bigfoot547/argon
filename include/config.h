#pragma once

#include <string>
#include <set>
#include "macros.h"

typedef unsigned long long ConfigParseResult;
#define MK_CONFIG_VAL(_line, _res) (unsigned long long)(((ConfigParseResult)(_line) << 32llu) | ((ConfigParseResult)(_res) & 0x00000000ffffffffllu))
#define CONFIG_GET_LINE_NUM(_val) (unsigned int)((ConfigParseResult)(_val) >> 32llu)
#define CONFIG_GET_RES_NUM(_val) (unsigned int)((ConfigParseResult)(_val) & 0x00000000ffffffffllu)

#define CONFIG_ERR_SUCCESS               0u
#define CONFIG_ERR_FILE_NOT_FOUND        1u
#define CONFIG_ERR_NO_PERMISSION         2u
#define CONFIG_ERR_FILE_UNKNOWN          3u
#define CONFIG_ERR_INVALID_IDENTIFIER    100u
#define CONFIG_ERR_NESTED_NAMESPACE      101u
#define CONFIG_ERR_VAL_OUTSIDE_NAMESPACE 102u
#define CONFIG_ERR_NAMESPACE_REDEFINED   103u
#define CONFIG_ERR_INVALID_OPERATOR      104u
#define CONFIG_ERR_EOL_IN_STRING         105u
#define CONFIG_ERR_EOF_IN_STRING         106u
#define CONFIG_ERR_NAMESPACE_NOT_FOUND   107u
#define CONFIG_ERR_EOF_IN_NAMESPACE      108u

#define CONFIG_ERR_UNQUOTED_CHARS        200u

class Configuration;
class ConfigurationNamespace;

// Directives are my cheap and stupid solution to being too lazy to implement string lists
class ConfigurationDirective {
	friend Configuration;

private:
	std::string key;
	std::string value;

public:
	ConfigurationDirective(std::string key, const std::string& rawValue);

	inline const std::string& GetDirective() const
	{ return key; }

	inline const std::string& GetValue() const
	{ return value; }
};

class ConfigurationItem
{
	friend Configuration;

private:
	const std::string name;
	const std::string literal;
	const ConfigurationNamespace* parent;

	// Cached values so we don't have to re evaluate them every time
	std::string strValue;
	int intValue;
	long longValue;

	bool haveStrValue, haveIntValue, haveLongValue;

public:
	NO_CLASS_COPY(ConfigurationItem)

	ConfigurationItem(std::string name, std::string literal, ConfigurationNamespace* parent) :
			name(std::move(name)),
			literal(std::move(literal)),
			parent(parent),
			intValue(0),
			longValue(0),
			haveStrValue(false),
			haveIntValue(false),
			haveLongValue(false)
	{}

	inline const std::string& GetName() const
	{ return name; }

	const std::string& GetString();

	int GetInt();

	long GetLong();
};

class ConfigurationNamespace
{
	friend Configuration;

private:
	const std::string name;
	std::set<ConfigurationItem*> items;

public:
	NO_CLASS_COPY(ConfigurationNamespace)

	explicit ConfigurationNamespace(std::string name) :
			name(std::move(name)),
			items()
	{}

	~ConfigurationNamespace();

	inline const std::string& GetName() const
	{ return name; }

	ConfigurationItem* GetItem(const std::string& key);

	std::string GetString(const std::string& key, const std::string& def = "");

	int GetInt(const std::string& key, int def = 0);

	long GetLong(const std::string& key, long def = 0);
};

class Configuration
{
private:
	const std::string configFile;
	std::set<ConfigurationNamespace*> namespaces;
	std::set<ConfigurationDirective*> directives;

protected:
	void Clear();

public:
	NO_CLASS_COPY(Configuration)

	explicit Configuration(std::string configFile) :
			configFile(std::move(configFile)),
			namespaces(),
			directives()
	{}

	~Configuration();

	bool VerifyConfig() const;

	ConfigParseResult ParseConfig();

	ConfigurationNamespace* GetNamespace(const std::string& name) const;

	std::set<ConfigurationDirective*> GetDirective(const std::string& name) const;

	static std::string GetErrorString(ConfigParseResult result);

	static std::string ParseStringFromLiteral(const std::string& literal);
};
