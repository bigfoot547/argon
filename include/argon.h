#pragma once

#include "config.h"
#include "modulemanager.h"

class Argon
{
private:
	Configuration* config;
	ModuleManager* moduleManager;

public:
	explicit Argon(Configuration* config) :
			config(config),
			moduleManager(new ModuleManager())
	{}

	~Argon();

	inline Configuration* GetConfig() const
	{ return config; }

	void InitModules();
};

extern Argon* ServInstance;
