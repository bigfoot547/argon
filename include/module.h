#pragma once

#include <string>
#include "macros.h"
#include "buildconf.h"

#define MOD_INIT_SYM_FUN1(x, y) MOD_INIT_SYM_FUN2(x, y)
#define MOD_INIT_SYM_FUN2(x, y) argon_module_ ## x ## _api_ ## y

#define MOD_INIT_STR_FUN1(x) MOD_INIT_STR_FUN2(x)
#define MOD_INIT_STR_FUN2(x) #x

#define MOD_INIT_SYM MOD_INIT_SYM_FUN1(VERSION_MAJOR, API_VERSION)
#define MOD_INIT_SYM_STR MOD_INIT_STR_FUN1(MOD_INIT_SYM)

#define MODULE_INIT(_c) \
extern "C" ModuleExport Module* MOD_INIT_SYM() { return new _c(); }

enum ServiceType
{
	SP_MODULE,
	SP_PROTOCOL,
	SP_SERVICE
};

class Module;

class ModuleException : public std::exception
{
private:
	const Module* module;
	const std::string details;

public:
	explicit ModuleException(std::string details = "No details specified.", const Module* module = nullptr) :
			module(module),
			details(std::move(details))
	{}

	const char* what() const noexcept override;
};

class ModuleSO
{
private:
	void* handle;
	const std::string name;
	std::string filename;

public:
	NO_CLASS_COPY(ModuleSO)

	explicit ModuleSO(std::string name);

	~ModuleSO();

	Module* Open();
};

class ServiceProvider
{
private:
	const ServiceType type;
	Module* parent;

public:
	NO_CLASS_COPY(ServiceProvider)

	ServiceProvider(ServiceType type, Module* parent) :
			type(type),
			parent(parent)
	{}

	virtual ~ServiceProvider() = default;

	inline ServiceType GetType() const
	{ return type; }

	inline Module* GetParent() const
	{ return parent; }
};

class Module : public ServiceProvider
{
private:
	ModuleSO* object;
	std::string name;

	friend ModuleSO;
public:
	NO_CLASS_COPY(Module)

	Module() :
			ServiceProvider(SP_MODULE, this),
			object(nullptr)
	{}

	~Module() override;

	inline const std::string& GetName() const
	{ return name; }

	virtual void Initialize() = 0;

	virtual void ReadConfig()
	{}
};
