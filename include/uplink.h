#pragma once

#include <string>
#include <thread>
#include <cstdio>
#include <sys/socket.h>

enum SocketState
{
	SS_NEW,
	SS_CONNECTED,
	SS_DISCONNECTED
};

class SocketException : public std::exception
{
private:
	const std::string whatstr;

public:
	explicit SocketException(std::string what) :
			whatstr(std::move(what))
	{}

	inline const char *what() const noexcept override
	{ return whatstr.c_str(); }
};

#define READBUF_SIZE 1024
class Socket
{
private:
	SocketState state;
	FILE *read, *write;
	int fd1, fd2;
	char *readbuf;

public:
	Socket() :
			state(SS_NEW),
			read(nullptr),
			write(nullptr),
			fd1(-1),
			fd2(-1),
			readbuf(new char[READBUF_SIZE])
	{}

	~Socket();

	inline SocketState GetState() const
	{ return state; }

	void Connect(const std::string& hostname, unsigned short port, bool ipv6 = true);

	void Write(const std::string& line);

	void WriteLine(const std::string& line);

	void Write(const char *data);

	void WriteLine(const char *data);

	void Flush();

	std::string ReadLine();
};

class Uplink
{
private:
	std::thread thread;

public:

};
