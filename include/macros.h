#define ModuleExport __attribute__ ((visibility("default")))

#define NO_CLASS_COPY(_c)          \
_c(_c const&) = delete;            \
_c& operator=(_c const&) = delete;

#define DO_IF_FALSE(_expr, _op) if (!(_expr)) _op(_expr);
