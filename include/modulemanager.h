#pragma once

#include <set>
#include <string>
#include "module.h"

class ModuleManager
{
private:
	std::set<ServiceProvider*> registeredProviders;

public:
	ModuleManager() :
			registeredProviders()
	{}

	void LoadModule(const std::string& name);

	Module* GetModule(const std::string& name);

	void UnloadModule(const std::string& name);

	void RegisterService(ServiceProvider* provider);

	void UnregisterService(ServiceProvider* provider);
};
