#include "modulemanager.h"

#include "module.h"

void ModuleManager::LoadModule(const std::string& name)
{
	auto* sharedObject = new ModuleSO(name);

	Module* mod = sharedObject->Open();
	registeredProviders.emplace(mod);
	mod->Initialize();
}

Module* ModuleManager::GetModule(const std::string& name)
{
	for (ServiceProvider* provider : registeredProviders)
	{
		if (provider->GetType() == SP_MODULE)
		{
			auto* mod = dynamic_cast<Module*>(provider);
			if (mod != nullptr && mod->GetName() == name)
			{
				return mod;
			}
		}
	}
	return nullptr;
}

void ModuleManager::UnloadModule(const std::string& name)
{
	auto* module = GetModule(name);
	registeredProviders.erase(module);
	delete module;
}

void ModuleManager::RegisterService(ServiceProvider* service)
{
	if (service->GetType() == SP_MODULE)
	{
		throw ModuleException("You may not use ModuleManager::RegisterService for modules. Use ModuleManager::LoadModule instead.");
	}

	registeredProviders.emplace(service);
}

void ModuleManager::UnregisterService(ServiceProvider* provider)
{
	registeredProviders.erase(provider);
	delete provider;
}
