#include "argon.h"
#include "buildconf.h"

#include <iostream>
#include "uplink.h"

Argon* ServInstance = nullptr;

int main()
{
	auto* config = new Configuration(COMPDIR "/etc/argon.conf");
	std::cout << "Loading configuration..." << std::endl;
	ConfigParseResult result = config->ParseConfig();

	if (CONFIG_GET_RES_NUM(result) == CONFIG_ERR_SUCCESS)
	{
		std::cout << "Configuration loaded: " << Configuration::GetErrorString(result) << "!" << std::endl;
	}
	else
	{
		std::cerr << "Error loading configuration: " << Configuration::GetErrorString(result) << "." << std::endl;
		return 1;
	}

	ServInstance = new Argon(config);

	ServInstance->InitModules();
	delete ServInstance;

	try
	{
		auto *abc = new Socket();
		abc->Connect("localhost", 6667, false);

		abc->WriteLine("NICK test");
		abc->WriteLine("USER ident unused unused realname");
		abc->Flush();

		while (abc->GetState() == SS_CONNECTED)
		{
			std::cout << abc->ReadLine() << std::endl;
		}
		std::cout << abc->GetState();

		delete abc;
	}
	catch (const SocketException& ex)
	{
		std::cerr << "SocketException: " << ex.what() << std::endl;
	}
	return 0;
}
