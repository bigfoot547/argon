#include "uplink.h"
#include "macros.h"

#include <netdb.h>
#include <arpa/inet.h>
#include <sstream>
#include <iostream>
#include <unistd.h>
#include <cstring>

void Socket::Connect(const std::string& hostname, unsigned short port, bool ipv6)
{
	struct addrinfo *res, hints{};
	struct addrinfo *original;
	int err;
	struct sockaddr *address = nullptr;

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_CANONNAME;

	if ((err = getaddrinfo(hostname.c_str(), nullptr, &hints, &original)) != 0)
	{
		throw SocketException("getaddrinfo: " + std::to_string(err) + ": " + std::string(gai_strerror(err)));
	}

	res = original;
	while (res)
	{
		if (res->ai_family == AF_INET || (res->ai_family == AF_INET6 && ipv6))
		{
			address = res->ai_addr;
		}

		if (address != nullptr) break;
		res = res->ai_next;
	}

	if (address == nullptr)
	{
		throw SocketException("Address is nullptr");
	}

	fd1 = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (fd1 == -1)
	{
		throw SocketException("socket: " + std::string(strerror(errno)));
	}

	auto *addresscopy = (struct sockaddr *)malloc(res->ai_addrlen);
	memcpy(addresscopy, address, res->ai_addrlen);

	if (res->ai_family == AF_INET)
	{
		reinterpret_cast<struct sockaddr_in *>(addresscopy)->sin_port = htons(port);
	}
	else // res->ai_family == AF_INET6
	{
		reinterpret_cast<struct sockaddr_in6 *>(addresscopy)->sin6_port = htons(port);
	}

	err = connect(fd1, addresscopy, res->ai_addrlen);

	freeaddrinfo(original);

	if (err == -1)
	{
		throw SocketException("connect: " + std::string(strerror(errno)));
	}

	delete addresscopy; // Is this allowed?

	fd2 = dup(fd1);
	read = fdopen(fd1, "r");
	write = fdopen(fd2, "w");

	state = SS_CONNECTED;
}

Socket::~Socket()
{
	DO_IF_FALSE(read, fclose)
	DO_IF_FALSE(write, fclose)

	if (fd1 != -1)
		close(fd1);

	if (fd2 != -1)
		close(fd2);

	delete readbuf;
}

void Socket::Write(const std::string& line)
{
	Write(line.data());
}

void Socket::WriteLine(const std::string& line)
{
	WriteLine(line.data());
}

void Socket::Write(const char *data)
{
	if (fputs(data, write) == EOF)
	{
		state = SS_DISCONNECTED;
		throw SocketException("Write error");
	}
}

void Socket::WriteLine(const char *data)
{
	Write(data);
	Write("\n");
}

void Socket::Flush()
{
	fflush(write);
}

std::string Socket::ReadLine() // WEE-OO-EE-OO! I don't think this is thread safe
{
	std::stringstream ss;
	if (!fgets(readbuf, READBUF_SIZE, read) || ferror(read))
	{
		state = SS_DISCONNECTED;
		throw SocketException("Read error");
	}

	for (int i = (int)strlen(readbuf) - 1; i >= 0; --i)
	{
		char c = readbuf[i];
		if (c == '\n' || c == '\r')
			readbuf[i] = 0;
		else
			break;
	}

	ss << readbuf;
	return ss.str();
}
