#include "buildconf.h"
#include "module.h"
#include <dlfcn.h>

const char* ModuleException::what() const noexcept
{
	if (module == nullptr)
		return details.c_str();
	else
		return std::string("In module " + module->GetName() + ": " + details).c_str();
}

ModuleSO::ModuleSO(std::string name) :
		handle(nullptr),
		name(std::move(name))
{
	filename = COMPDIR "/obj/modules/" + this->name + ".so"; // Change this to edit where module shared objects are
}

ModuleSO::~ModuleSO()
{
	if (handle)
	{
		dlclose(handle);
	}
}

Module* ModuleSO::Open()
{
	dlerror();
	handle = dlopen(filename.c_str(), RTLD_NOW | RTLD_LOCAL); // NOLINT(hicpp-signed-bitwise)

	if (handle == nullptr)
		throw ModuleException("Unable to load module " + name + ": " + std::string(dlerror()));

	Module* newModule = nullptr;

	dlerror();
	auto* init_sym = reinterpret_cast<Module* (*)()>(dlsym(handle, MOD_INIT_SYM_STR));
	char* error = dlerror();
	if (error != nullptr)
		throw ModuleException("Error getting module symbol (" + name + "): " + std::string(error));

	newModule = init_sym();
	if (newModule == nullptr)
		throw ModuleException("Module pointer is null! (" + name + ")");

	newModule->name = name;
	newModule->object = this;
	return newModule;
}

Module::~Module()
{
	delete object;
}
