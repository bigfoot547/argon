#include "argon.h"
#include "module.h"

#include <iostream>

Argon::~Argon()
{
	delete config;
	delete moduleManager;
}

void Argon::InitModules()
{
	for (ConfigurationDirective* directive : config->GetDirective("loadmodule"))
	{
		try
		{
			moduleManager->LoadModule(directive->GetValue());
		}
		catch (const ModuleException& ex)
		{
			std::cerr << ex.what() << std::endl;
			continue;
		}
		std::cout << "Successfully loaded module " << directive->GetValue() << "." << std::endl;
	}
}
