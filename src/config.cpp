#include "config.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <cerrno>
#include <sys/stat.h>
#include <cstring>

ConfigurationDirective::ConfigurationDirective(std::string key, const std::string& rawValue) :
		key(std::move(key))
{
	value = Configuration::ParseStringFromLiteral(rawValue);
}

const std::string& ConfigurationItem::GetString()
{
	if (haveStrValue) return strValue;

	strValue = Configuration::ParseStringFromLiteral(literal);
	haveStrValue = true;
	return strValue;
}

int ConfigurationItem::GetInt()
{
	if (haveIntValue) return intValue;

	try
	{
		intValue = std::stoi(literal);
		haveIntValue = true;
	}
	catch (const std::exception& ex)
	{
		return 0;
	}
	return intValue;
}

long ConfigurationItem::GetLong()
{
	if (haveLongValue) return longValue;

	try
	{
		longValue = std::stol(literal);
		haveLongValue = true;
	}
	catch (const std::exception& ex)
	{
		return 0;
	}
	return longValue;
}

ConfigurationNamespace::~ConfigurationNamespace()
{
	for (ConfigurationItem* item : items)
	{
		delete item;
	}
}

ConfigurationItem* ConfigurationNamespace::GetItem(const std::string& key)
{
	for (ConfigurationItem* item : items)
	{
		if (item->GetName() == key)
			return item;
	}
	return nullptr;
}

std::string ConfigurationNamespace::GetString(const std::string& key, const std::string& def)
{
	ConfigurationItem* item = GetItem(key);

	if (item != nullptr)
		return item->GetString();
	return def;
}

int ConfigurationNamespace::GetInt(const std::string& key, int def)
{
	ConfigurationItem* item = GetItem(key);

	if (item != nullptr)
		return item->GetInt();
	return def;
}

long ConfigurationNamespace::GetLong(const std::string& key, long def)
{
	ConfigurationItem* item = GetItem(key);

	if (item != nullptr)
		return item->GetLong();
	return def;
}

void Configuration::Clear()
{
	for (ConfigurationNamespace* cn : namespaces)
	{
		delete cn;
	}

	for (ConfigurationDirective* cd : directives)
	{
		delete cd;
	}

	namespaces.clear();
	directives.clear();
}

Configuration::~Configuration()
{
	for (ConfigurationNamespace* cn : namespaces)
	{
		delete cn;
	}

	for (ConfigurationDirective* cd : directives)
	{
		delete cd;
	}
}

bool Configuration::VerifyConfig() const
{
	struct stat s{};
	return stat(configFile.c_str(), &s) != -1;
}

ConfigParseResult Configuration::ParseConfig()
{
	if (!VerifyConfig())
	{
		switch (errno)
		{
			case EPERM:
				return MK_CONFIG_VAL(0, CONFIG_ERR_NO_PERMISSION);
			case ENOENT:
				return MK_CONFIG_VAL(0, CONFIG_ERR_FILE_NOT_FOUND);
			default:
				return MK_CONFIG_VAL(errno, CONFIG_ERR_FILE_UNKNOWN);
		}
	}

	Clear();

	std::ifstream infile(configFile);
	if (!infile.is_open())
	{
		return MK_CONFIG_VAL(errno, CONFIG_ERR_FILE_UNKNOWN);
	}

	ConfigurationNamespace* curNamespace = nullptr;
	std::stringstream identifier;
	std::stringstream data;

	enum ParseState
	{
		PARSE_IDENTIFIER,
		PARSE_OPERATOR,
		PARSE_DATA
	};

	ParseState state = PARSE_IDENTIFIER;

#define NEW_LINE               \
identifier.str(std::string()); \
data.str(std::string());       \
inQuotes = false;              \
couldBeString = false;         \
inComment = false;             \
state = PARSE_IDENTIFIER;      \
NEXT_CHAR

#define NEXT_CHAR   \
continue;

	char c;
	int lineNum = 1;
	bool inQuotes = false, inComment = false;
	bool couldBeString = false;
	bool escaped = false;
	while ((c = infile.get()) != EOF)
	{
		if (c == '\n')
		{
			if (inQuotes)
			{
				return MK_CONFIG_VAL(lineNum, CONFIG_ERR_EOL_IN_STRING);
			}
			inQuotes = false;
			inComment = false;

			++lineNum;
		}
		if (c == '\n' || c == '\r') c = ' '; // Newline characters are whitespace

		if (inComment)
		{
			NEXT_CHAR
		}

		//std::cout << c << " - " << state << " - " << data.str() << " " << inQuotes << std::endl;

		if (state == PARSE_IDENTIFIER)
		{
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_' || c == '-')
			{
				identifier << c;
			}
			else if (c == ' ' || c == '\t')
			{
				if (identifier.str().length() == 0)
				{
					NEXT_CHAR
				}
				else
				{
					state = PARSE_OPERATOR;
					NEXT_CHAR
				}
			}
			else if (c == '#' && identifier.str().length() == 0)
			{
				inComment = true;
			}
			else if (c == '}' && identifier.str().length() == 0)
			{
				if (curNamespace == nullptr)
				{
					return MK_CONFIG_VAL(lineNum, CONFIG_ERR_NAMESPACE_NOT_FOUND);
				}
				else
				{
					curNamespace = nullptr;
				}
			}
			else
			{
				return MK_CONFIG_VAL(lineNum, CONFIG_ERR_INVALID_IDENTIFIER);
			}
		}
		else if (state == PARSE_OPERATOR)
		{
			if (c == ' ' || c == '\t')
			{
				NEXT_CHAR
			}
			else if (c == '{') // Start of a namespace
			{
				if (curNamespace == nullptr)
				{
					const std::string nsName = identifier.str();

					if (GetNamespace(nsName))
					{
						return MK_CONFIG_VAL(lineNum, CONFIG_ERR_NAMESPACE_REDEFINED);
					}
					else
					{
						curNamespace = new ConfigurationNamespace(nsName);
						namespaces.emplace(curNamespace);
						NEW_LINE
					}
				}
				else
				{
					return MK_CONFIG_VAL(lineNum, CONFIG_ERR_NESTED_NAMESPACE);
				}
			}
			else if (c == '=')
			{
				if (curNamespace == nullptr)
				{
					return MK_CONFIG_VAL(lineNum, CONFIG_ERR_VAL_OUTSIDE_NAMESPACE);
				}
				else
				{
					state = PARSE_DATA;
					NEXT_CHAR
				}
			}
			else
			{
				if (curNamespace == nullptr) // Could be a directive
				{
					state = PARSE_DATA;
					infile.unget();
					NEXT_CHAR
				}
				else
				{
					return MK_CONFIG_VAL(lineNum, CONFIG_ERR_INVALID_OPERATOR);
				}
			}
		}
		else if (state == PARSE_DATA)
		{
			if (!inQuotes && c == ';')
			{
				//std::cout << "Line ended! Identifier: \"" << identifier.str() << "\" - Data: \"" << data.str() << "\""
				//		  << std::endl;
				if (curNamespace == nullptr)
				{
					directives.emplace(new ConfigurationDirective(identifier.str(), data.str()));
				}
				else // Not a directive
				{
					curNamespace->items.emplace(new ConfigurationItem(identifier.str(), data.str(), curNamespace));
				}
				NEW_LINE
			}
			else if (!inQuotes && c == '#')
			{
				inComment = true;
				NEXT_CHAR
			}

			if (inQuotes)
			{
				if (!escaped && c == '\"')
					inQuotes = false;

				if (c == '\\' && !escaped)
				{
					escaped = true;
					data << c;
					NEXT_CHAR
				}
			}
			else
			{
				if (c == '\"')
				{
					inQuotes = true;
					couldBeString = true;
				}
				else if (c == ' ' || c == '\t')
				{
					NEXT_CHAR
				}
				else
				{
					if (couldBeString)
						return MK_CONFIG_VAL(lineNum, CONFIG_ERR_UNQUOTED_CHARS);
				}
			}

			if (data.str().length() > 0 || (c != ' ' && c != '\t'))
				data << c;
			escaped = false;
		}
	}

#undef NEW_LINE
#undef NEXT_CHAR

	if (inQuotes)
		return MK_CONFIG_VAL(lineNum, CONFIG_ERR_EOF_IN_STRING);
	if (curNamespace != nullptr)
		return MK_CONFIG_VAL(lineNum, CONFIG_ERR_EOF_IN_NAMESPACE);

	return MK_CONFIG_VAL(0, CONFIG_ERR_SUCCESS);
}

ConfigurationNamespace* Configuration::GetNamespace(const std::string& name) const
{
	for (ConfigurationNamespace* ns : namespaces)
	{
		if (ns->GetName() == name)
		{
			return ns;
		}
	}
	return nullptr;
}

std::set<ConfigurationDirective*> Configuration::GetDirective(const std::string& name) const // Will always copy the set
{
	if (name.empty())
	{
		return std::set<ConfigurationDirective*>(directives);
	}

	std::set<ConfigurationDirective*> ret;
	for (ConfigurationDirective* directive : directives)
	{
		if (directive->key == name)
		{
			ret.emplace(directive);
		}
	}
	return ret;
}

std::string Configuration::GetErrorString(ConfigParseResult result)
{
	unsigned int linenum = CONFIG_GET_LINE_NUM(result);
	std::ostringstream os;

	switch (CONFIG_GET_RES_NUM(result))
	{
		case CONFIG_ERR_SUCCESS:
			os << "Success";
			break;
		case CONFIG_ERR_FILE_NOT_FOUND:
			os << "File not found";
			break;
		case CONFIG_ERR_NO_PERMISSION:
			os << "Permission denied";
			break;
		case CONFIG_ERR_FILE_UNKNOWN:
			os << "Unknown I/O error";
			if (linenum != 0)
				os << ": " << strerror((int)linenum);
			break;
		case CONFIG_ERR_INVALID_IDENTIFIER:
			os << "Invalid character(s) in identifier on line " << linenum;
			break;
		case CONFIG_ERR_NESTED_NAMESPACE:
			os << "Illegal nested namespace on line " << linenum;
			break;
		case CONFIG_ERR_VAL_OUTSIDE_NAMESPACE:
			os << "Value defined outside of namespace on line " << linenum;
			break;
		case CONFIG_ERR_NAMESPACE_REDEFINED:
			os << "Namespace defined on line " << linenum << " has previously been defined";
			break;
		case CONFIG_ERR_INVALID_OPERATOR:
			os << "Invalid operator used on line " << linenum;
			break;
		case CONFIG_ERR_EOL_IN_STRING:
			os << "EOL in string literal on line " << linenum;
			break;
		case CONFIG_ERR_EOF_IN_STRING:
			os << "Unexpected end of file in string literal on line " << linenum;
			break;
		case CONFIG_ERR_NAMESPACE_NOT_FOUND:
			os << "No namespace to end on line " << linenum;
			break;
		case CONFIG_ERR_EOF_IN_NAMESPACE:
			os << "Unclosed namespace by end of file on line " << linenum;
			break;
		case CONFIG_ERR_UNQUOTED_CHARS:
			os << "Unquoted characters present in apparent string literal on line " << linenum;
			break;
		default:
			os << "Unknown error (" << result << ")";
	}
	return os.str();
}

std::string Configuration::ParseStringFromLiteral(const std::string& literal)
{
	std::stringstream ret;

	bool inQuotes = false, escaped = false;
	for (char c : literal)
	{
		if (inQuotes)
		{
			if (escaped)
			{
				switch (c)
				{
					case 't':
						ret << '\t';
						break;
					case 'n':
						ret << '\n';
						break;
					case 'r':
						ret << '\r';
						break;
					default:
						ret << c;
				}
				escaped = false;
			}
			else
			{
				if (c == '\"')
				{
					inQuotes = false;
					continue;
				}
				else if (c == '\\')
				{
					escaped = true;
					continue;
				}
				ret << c;
			}
		}
		else
		{
			if (c == ' ' || c == '\t')
				continue;

			if (c == '\"')
			{
				inQuotes = true;
			}
		}
	}
	return ret.str();
}
